/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;
import java.sql.*;
/**
 *
 * @author Daniel
 */
public class Conexion {
    private static Connection conexion=null;
       
    private Conexion(){  }
    
    public static Connection getInstance(){
        if(conexion==null){
            
            try {
                DriverManager.registerDriver(new com.mysql.jdbc.Driver());
                conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa2","root","");
                System.out.println("Conexion exitosa");
            } catch (SQLException e) {
                System.out.println("**************************************");
                System.out.println("**************************************");
                System.out.println("**************************************");
                System.out.println("**************************************");
                System.out.println("**************************************");
                System.out.println("**************************************");
                System.out.println("Error: "+e);
                System.out.println("Error al registrar el controlador"+ e.getMessage());
            }
        }
         return conexion;
    }
}
