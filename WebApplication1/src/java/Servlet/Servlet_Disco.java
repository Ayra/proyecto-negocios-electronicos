/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Clase.Disco;
import Clase.DetalleVenta2;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Clase.*;
import Clase.DAOImpl.DiscoDAOImpl;
import Conexion.Conexion;
import Facade.FacadeMensaje;
import java.sql.*;
import java.text.*;
import java.util.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
/**
 *
 * @author Daniel
 */
@WebServlet(name = "Servlet_Disco", urlPatterns = {"/Servlet_Disco"})
public class Servlet_Disco extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String accion = request.getParameter("accion");
            
            if(accion.equals("insertar")){
                this.RegistrarDisco(request, response); 
            }
            if(accion.equals("actualizar")){
                this.ActualizarDisco(request, response); 
            }
            if(accion.equals("eliminar")){
                this.DarBajaDisco(request, response); 
            }
            if(accion.equals("recuperar")){
                this.DarAltaDisco(request, response); 
            }
            if(accion.equals("anadirCarrito")){
                this.AnadirCarrito(request, response);
            }
        }
    }
    
    private void RegistrarDisco (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Disco p = new Disco();
        p.setGeneroM(request.getParameter("txtClase")); 
        p.setArtista(request.getParameter("txtMarca"));
        p.setDescripcion(request.getParameter("txtDescripcion"));
        p.setPrecioD(Double.parseDouble(request.getParameter("txtPrecio")));
        p.setImagenD(request.getParameter("txtImagen")); 
        p.setStockD(Double.parseDouble(request.getParameter("txtStock")));
        
        boolean resp = DiscoDAOImpl.getInstance().insertarDisco(p);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha registrado un disco correctamente'", 1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al registrar un disco'", 1);
            response.sendRedirect(facade.print());
        }
    }
    
    private void ActualizarDisco(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Disco p = new Disco();
        p.setCodigoD(request.getParameter("txtCodigo")); 
        p.setGeneroM(request.getParameter("txtNombreGM")); 
        p.setArtista(request.getParameter("txtNombreA"));
        p.setDescripcion(request.getParameter("txtDescripcionD"));
        p.setPrecioD(Double.parseDouble(request.getParameter("txtPrecioD")));
        p.setStockD(Double.parseDouble(request.getParameter("txtStockD")));
        
        String imagen = request.getParameter("selected");
        
        if(imagen.equals("SelectImagenActual")){
            p.setImagenD(request.getParameter("txtImagen")); 
        }else{
            p.setImagenD(request.getParameter("txtModificarImagen")); 
        }
        //response.sendRedirect("mensaje.jsp?mens='Error al modificar el disco"+p.getDescripcion()+"'");
        boolean resp = DiscoDAOImpl.getInstance().actualizarDisco(p);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha modificado el disco "+p.getCodigoD()+"'", 1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al modificar el disco'", 1);
            response.sendRedirect(facade.print()); 
        }
    }
    
    private void DarBajaDisco(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Disco p = new Disco();
        p.setCodigoD(request.getParameter("codigoD")); 
        
        boolean resp = DiscoDAOImpl.getInstance().eliminarDisco(p);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha eliminado el disco "+p.getCodigoD()+"'", 1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al eliminar el disco'", 1);
            response.sendRedirect(facade.print()); 
        }
    }
    
    private void DarAltaDisco(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Disco p = new Disco();
        p.setCodigoD(request.getParameter("codigoD")); 
        
        boolean resp = DiscoDAOImpl.getInstance().recuperarDisco(p);
        if(resp){
            response.sendRedirect("mensaje.jsp?mens='Se ha recuperado el disco "+p.getCodigoD()+"'"); 
        }else{
            response.sendRedirect("mensaje.jsp?mens='Error al recuperar el disco'"); 
        }
    }
    
    
    private void AnadirCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        
        HttpSession session = request.getSession();
        ArrayList<DetalleVenta2> carritoCompra;
        
        //si no esta creado el carrito lo crea, sino carga el carrito creado anteriormente
        if(session.getAttribute("carrito") == null){
            carritoCompra = new ArrayList<DetalleVenta2>();
        }else{
            carritoCompra = (ArrayList<DetalleVenta2>)session.getAttribute("carrito");
        }
        
        Disco p = DiscoDAOImpl.getInstance().listarDiscoPorCodigo(request.getParameter("txtCodigo")); 
        
        //Crea un nuevo detalle venta
        DetalleVenta2 dv = new DetalleVenta2();
        dv.setCodigoDisco(request.getParameter("txtCodigo")); 
        dv.setNombreDisco(request.getParameter("txtNombreD")); 
        dv.setPrecio(Double.parseDouble(request.getParameter("txtPrecio")));
        dv.setCantidad(Double.parseDouble(request.getParameter("txtCantidad"))); 
        
        // Realizamos un descuento para el sub total
        double subTotal = p.getPrecioD() * dv.getCantidad();
        if(subTotal > 1000){
            dv.setDescuento(subTotal * 0.25);
        }
        else if(subTotal > 400){
            dv.setDescuento(subTotal * 0.10);
        }
        else{
            dv.setDescuento(0); 
        }
        
        dv.setSubTotal(dv.getPrecio() * dv.getCantidad() - dv.getDescuento());
        
        //busca si nuevamente queremos añadir al carrito un producto que ya esta en el carrito
        int indice = -1;
        for(int i=0; i<carritoCompra.size(); i++){
            DetalleVenta2 det = carritoCompra.get(i);
            if(det.getCodigoDisco().equals(p.getCodigoD())){
                indice = i;
                break;
            }
        }
        
        if(indice == -1){ //producto nuevo en el carrito
            dv.setNumero(String.valueOf(carritoCompra.size() + 1)); 
            carritoCompra.add(dv);
        }else{ //se modifica el producto que ya existe
            dv.setNumero(String.valueOf(indice + 1));
            carritoCompra.set(indice, dv);
        }
        
        session.setAttribute("carrito", carritoCompra); 
        request.getSession().setAttribute("parametroCliente", request.getParameter("txtCliente"));
        response.sendRedirect("RegistrarVenta.jsp");
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
