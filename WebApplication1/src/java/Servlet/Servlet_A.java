/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;


import Clase.Artista;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Clase.*;
import Clase.DAO.ArtistaDAO;
import Clase.DAOImpl.ArtistaDAOImpl;
import Facade.FacadeMensaje;
import javax.servlet.annotation.WebServlet;
/**
 *
 * @author Daniel
 */
@WebServlet(name = "Servlet_A", urlPatterns = {"/Servlet_A"})
public class Servlet_A extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String accion = request.getParameter("accion");
            
            if(accion.equals("insertar")){
                this.RegistrarArtista(request, response);
            }
            if(accion.equals("actualizar")){
                this.ActualizarArtista(request, response);
            }
            if(accion.equals("eliminar")){
                this.darBajaArtista(request, response);
            }
            if(accion.equals("recuperar")){
                this.darAltaArtista(request, response);
            }
        }
    }
    
    private void RegistrarArtista(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Artista mp = new Artista();
        mp.setNombreA(request.getParameter("txtNombre"));
        
        boolean resp = ArtistaDAOImpl.getInstance().insertarArtista(mp);
        //response.sendRedirect("mensaje.jsp?mens='Se ha registrado un artista correctamente");
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha registrado un artista correctamente'",1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al registrar un artista'",1);
            response.sendRedirect(facade.print()); 
        }
    }
    
    private void ActualizarArtista(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Artista mp = new Artista();
        mp.setCodigoA(request.getParameter("txtCodigo"));
        mp.setNombreA(request.getParameter("txtNombre")); 
        
        boolean resp = ArtistaDAOImpl.getInstance().actualizarArtista(mp);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha modificado al artista "+mp.getCodigoA()+"'",1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al modificar el artista'", 1);
            response.sendRedirect(facade.print()); 
        }
    }
    
    private void darBajaArtista(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Artista mp = new Artista();
        mp.setCodigoA(request.getParameter("codigoA"));
        
        boolean resp = ArtistaDAOImpl.getInstance().eliminarArtista(mp);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha eliminado al artista "+mp.getCodigoA()+"'",1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al eliminar el artista'", 1);
            response.sendRedirect(facade.print()); 
        }
    }
    
    private void darAltaArtista(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Artista mp = new Artista();
        mp.setCodigoA(request.getParameter("codigoA"));
        
        boolean resp = ArtistaDAOImpl.getInstance().recuperarArtista(mp);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha recuperado al artista "+mp.getCodigoA()+"'",1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al recuperar el artista'", 1);
            response.sendRedirect(facade.print()); 
        }
    }   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
