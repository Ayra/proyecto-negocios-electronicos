/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Clase.GeneroM;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Clase.*;
import Clase.DAOImpl.GeneroMDAOImpl;
import Facade.FacadeMensaje;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author Daniel
 */
@WebServlet(name = "Servlet_GM", urlPatterns = {"/Servlet_GM"})
public class Servlet_GM extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            String accion = request.getParameter("accion");
            if(accion.equals("insertar")){
                this.RegistrarGeneroMusical(request, response);
            }
            if(accion.equals("actualizar")){
                this.ActualizarGeneroMusical(request, response); 
            }
            if(accion.equals("eliminar")){
                this.EliminarGeneroMusical(request, response); 
            }
            if(accion.equals("recuperar")){
                this.RecuperarGeneroMusical(request, response); 
            }
            
        }
    }
    
    private void RegistrarGeneroMusical(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        GeneroM cp = new GeneroM();
        cp.setNombreGM(request.getParameter("txtNombre")); 
        
        boolean resp = GeneroMDAOImpl.getInstance().insertarGeneroMusical(cp);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha registrado un genero musical correctamente'", 1);
            response.sendRedirect(facade.print());
        }else{
            facade=new FacadeMensaje("'Error al registrar en la clase genero musical'", 1);
            response.sendRedirect(facade.print()); 
        }
    }
    
    private void ActualizarGeneroMusical(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        GeneroM cp = new GeneroM();
        cp.setCodigoGM(request.getParameter("txtCodigo"));
        cp.setNombreGM(request.getParameter("txtNombre")); 
        
        boolean resp = GeneroMDAOImpl.getInstance().actualizarGeneroMusical(cp);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha modificado el genero musical "+cp.getCodigoGM()+"'", 1);
            response.sendRedirect(facade.print());
        }else{
            facade=new FacadeMensaje("'Error al actualizar el genero musical'", 1);
            response.sendRedirect(facade.print());
        }
    }
    
    private void EliminarGeneroMusical (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        GeneroM cp = new GeneroM();
        cp.setCodigoGM(request.getParameter("codigoGM"));
        
        boolean resp = GeneroMDAOImpl.getInstance().darBajaGeneroMusical(cp);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha eliminado el genero musical "+cp.getCodigoGM()+"'", 1);
            response.sendRedirect(facade.print());
        }else{
            facade=new FacadeMensaje("'Error al eliminar el genero musical'", 1);
            response.sendRedirect(facade.print());
        }
    }
    
    private void RecuperarGeneroMusical(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        GeneroM cp = new GeneroM();
        cp.setCodigoGM(request.getParameter("codigoGM"));
        
        boolean resp = GeneroMDAOImpl.getInstance().darAltaGeneroMusical(cp);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha recuperado el genero musical "+cp.getCodigoGM()+"'", 1);
            response.sendRedirect(facade.print());
        }else{
            facade=new FacadeMensaje("'Error al recuperar el genero musical'", 1);
            response.sendRedirect(facade.print());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
