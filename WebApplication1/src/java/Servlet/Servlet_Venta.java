/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Clase.Venta;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Conexion.Conexion;
import Clase.*;
import Clase.DAOImpl.VentaDAOImpl;
import Facade.FacadeMensaje;
import Strategy.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import static jdk.nashorn.internal.objects.NativeString.toUpperCase;
/**
 *
 * @author Daniel
 */
@WebServlet(name = "Servlet_Venta", urlPatterns = {"/Servlet_Venta"})
public class Servlet_Venta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String accion = request.getParameter("accion");
            
            if(accion.equals("RegistrarVenta")){
                this.RegistrarVenta(request, response); 
            }
            if(accion.equals("MostrarDetalle")){
                this.MostrarDetalleVenta(request, response); 
            }
            if(accion.equals("EliminarVenta")){
                this.EliminarVenta(request, response); 
            }
            if(accion.equals("quitar")){
                this.QuitarProducto(request, response); 
            }
        }
    }
    private void DisminuirStock(Connection cn,String Nombre,String Cantidad)
            throws ServletException, IOException {
        String Codigo_D="";
            String Nombre_GM="";
            String Nombre_A="";
            String Descripcion_D="";
            String Imagen_D="";
            double Precio_D=0;
            double Stock_D=0;
            String cadena="",a,b;
            CallableStatement cs ;
            int intIndex;
            ResultSet rs = null;
        try{
            a="-"; 
            b=toUpperCase(Nombre);

             intIndex = b.indexOf(a);
             cadena=b.substring (intIndex+2,b.length());

             cs = cn.prepareCall("CALL BUSCAR_DISCO_NOMBRE (?)");
             cs.setString(1, cadena);

             rs=cs.executeQuery();
             while(rs.next()){
             Codigo_D = rs.getString("Codigo_D");
             Nombre_GM = rs.getString("Nombre_GM");
             Nombre_A = rs.getString("Nombre_A");
             Descripcion_D = rs.getString("Descripcion_D");
             Imagen_D = rs.getString("Imagen_D");
             Precio_D = rs.getDouble("Precio_D");
             Stock_D = rs.getDouble("Stock_D");
             }

             cs = cn.prepareCall("CALL MODIFICAR_DISCO (?,?,?,?,?,?,?)");
             cs.setString(1, Codigo_D);
             cs.setString(2, Nombre_GM);
             cs.setString(3, Nombre_A);
             cs.setString(4, Descripcion_D);
             cs.setDouble(5, Precio_D);
             cs.setString(6, Imagen_D);
             cs.setDouble(7, Stock_D - Double.parseDouble(Cantidad));
             cs.executeUpdate();

         }catch(SQLException e){}
    }
    
    private void DisminuirSaldo(Connection cn,String Usuario,String Precio,String tipo)
            throws ServletException, IOException {
       
            CallableStatement cs;
            String Codigo_U="";
        try{
            String a=","; 
            String b=Usuario;

             int intIndex = b.indexOf(a);
             String cadena=b.substring (0,intIndex);

             cs = cn.prepareCall("CALL BUSCAR_USUARIO (?)");
             cs.setString(1, cadena);

             ResultSet rs=cs.executeQuery();
             while(rs.next()){
             Codigo_U = rs.getString("Codigo_U");
             
             }
             cs = cn.prepareCall("CALL MODIFICAR_SALDO_CUENTA (?,?,?)");
             cs.setString(1, Codigo_U);
             cs.setDouble(2, Double.parseDouble(Precio));
             cs.setString(3, tipo);
             cs.executeUpdate();

         }catch(SQLException e){}
    }
    
    private void RegistrarVenta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        ArrayList lista = (ArrayList)session.getAttribute("carrito");
        String CodigoCli = request.getParameter("txtCliente");
        String Pago = request.getParameter("ModoPago");
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        
        Connection cn;
        cn = Conexion.getInstance();
        
        Venta v = new Venta();
        v.setCliente(request.getParameter("txtCliente")); 
        v.setTotal(Double.parseDouble(request.getParameter("txtTotal")));
        
        
        boolean resp = VentaDAOImpl.getInstance().insertarVenta(v);
        FacadeMensaje facade;
        if(resp){
            // Regostrar detalle vemta
            String CodigoVenta = request.getParameter("txtCodigoV");
            String NombreDisco[] = request.getParameterValues("nombreDisco");
            String PrecioDisco[] = request.getParameterValues("precioDisco");
            String CantidadDisco[] = request.getParameterValues("cantidadDisco");
            String DescuentoDisco[] = request.getParameterValues("descuentoDisco");
            String SubTotalDisco[] = request.getParameterValues("subTotalDisco");
            
            if("null".equals(CodigoVenta)){
                CodigoVenta = "V0001";
            }
            CallableStatement cs ;
            Pago_Strategy StrPay = null;
            switch(Pago){
                case "MasterCard":StrPay = new MasterCard_Strategy();break;
                case "Visa":StrPay = new Visa_Strategy();break;
                case "Paypal":StrPay = new PayPal_Strategy();break;
                default:break;
            }
            Pago=StrPay.pagar();
            for(int i=0; i<NombreDisco.length;i++){
                this.DisminuirStock(cn,NombreDisco[i],CantidadDisco[i]);                
                this.DisminuirSaldo(cn, CodigoCli, SubTotalDisco[i], Pago);
                try{

                    cs = cn.prepareCall("CALL REGISTRAR_DETALLE_VENTA (?,?,?,?,?,?)");
                    cs.setString(1, CodigoVenta);
                    cs.setString(2, NombreDisco[i]);
                    cs.setString(3, PrecioDisco[i]);
                    cs.setString(4, CantidadDisco[i]);
                    cs.setString(5, DescuentoDisco[i]);
                    cs.setString(6, SubTotalDisco[i]);
                    int j = cs.executeUpdate();
                    if(j==1){
                        facade=new FacadeMensaje("'Se ha registrado su compra correctamente "+CodigoCli+"'", 2);
                        response.sendRedirect(facade.print()); 
                        lista.clear();
                    }else{
                        facade=new FacadeMensaje("'Error al registrar su compra'", 2);
                        response.sendRedirect(facade.print());
                    }
                }catch(Exception e){
                    facade=new FacadeMensaje("'Error al registrar su compra'", 2);
                    response.sendRedirect(facade.print());}
            }
        }else{
            facade=new FacadeMensaje("'Error al registrar su compra'", 2);
            response.sendRedirect(facade.print());
        }
    }
    
    private void MostrarDetalleVenta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        request.getSession().setAttribute("CodigoVenta", request.getParameter("codigoV"));
        request.getSession().setAttribute("Cliente", request.getParameter("cliente"));
        request.getSession().setAttribute("Importe", request.getParameter("importe"));
        request.getSession().setAttribute("FechaVenta", request.getParameter("FechaV")); 
        request.getSession().setAttribute("CodigoCliente", request.getParameter("codigoCli")); 

        response.sendRedirect("MostrarDetalleVenta.jsp"); 
    }
    
    private void QuitarProducto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(false); 
        
        int numeroVenta = Integer.parseInt(request.getParameter("numero"));
        ArrayList lista = (ArrayList)session.getAttribute("carrito");
        lista.remove(numeroVenta - 1); 
        response.sendRedirect("RegistrarVenta.jsp"); 
    }
    
    private void EliminarVenta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        Venta v = new Venta();
        v.setCodigoVenta(request.getParameter("codigoV"));
        boolean resp = VentaDAOImpl.getInstance().eliminarVenta(v);
        FacadeMensaje facade;
        if(resp){
            facade=new FacadeMensaje("'Se ha eliminado la venta correctamente "+ v.getCodigoVenta() +"'", 1);
            response.sendRedirect(facade.print()); 
        }else{
            facade=new FacadeMensaje("'Error al eliminar la venta " +v.getCodigoVenta()+ "'", 1);
            response.sendRedirect(facade.print()); 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
