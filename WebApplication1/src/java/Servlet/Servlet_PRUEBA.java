/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Clase.Artista;
import Clase.DAOImpl.ArtistaDAOImpl;
import Conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Joee
 */
@WebServlet(name = "Servlet_PRUEBA", urlPatterns = {"/Servlet_PRUEBA"})
public class Servlet_PRUEBA extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        
        String accion = request.getParameter("accion");
        
        if(accion.equals("insertar")){
                this.RegistrarArtista(request, response);
            }
        
        
        
        
    }
    
    private void RegistrarArtista(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        Connection cn;
        cn = Conexion.getInstance();
        CallableStatement cs ;
            ResultSet rs = null;
            cs = cn.prepareCall("BUSCAR_DISCO_NOMBRE (?)");
                    cs.setString(1, "SUENO");
        String Codigo_D="";
            String Nombre_GM="";
            String Nombre_A="";
            String Descripcion_D="";
            String Imagen_D="";
            double Precio_D=0;
            double Stock_D=0;
         rs=cs.executeQuery();
                    while(rs.next()){
                    Codigo_D = rs.getString("Codigo_D");
                    Nombre_GM = rs.getString("Nombre_GM");
                    Nombre_A = rs.getString("Nombre_A");
                    Descripcion_D = rs.getString("Descripcion_D");
                    Imagen_D = rs.getString("Imagen_D");
                    Precio_D = rs.getDouble("Precio_D");
                    Stock_D = rs.getDouble("Stock_D");
                    }
                    try (PrintWriter out = response.getWriter()) {
                    out.println("<h1>"+Codigo_D+"</h1>");
                    }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Servlet_PRUEBA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Servlet_PRUEBA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
