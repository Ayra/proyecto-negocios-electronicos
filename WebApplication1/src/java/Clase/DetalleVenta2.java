/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase;
import Interface.*;
import Decorator.*;
/**
 *
 * @author Daniel
 */
public class DetalleVenta2 extends ComponentDecorator{
    private String numero;
    private String CodigoVenta;
    private String codigoDisco;
    private String NombreDisco;
    private double Precio;
    private double Cantidad;
    private double Descuento;
    private double SubTotal;
    private IDisco Disco;
    private IVenta Venta;
    private CarritoCompra currentCarrito;
    
    public DetalleVenta2() {
    
    }

    public DetalleVenta2(String numero, String CodigoVenta, String codigoDisco, String NombreDisco, double Precio, double Cantidad, double Descuento, double SubTotal, IDisco Disco, IVenta Venta, CarritoCompra currentCarrito) {
        this.numero = numero;
        this.CodigoVenta = CodigoVenta;
        this.codigoDisco = codigoDisco;
        this.NombreDisco = NombreDisco;
        this.Precio = Precio;
        this.Cantidad = Cantidad;
        this.Descuento = Descuento;
        this.SubTotal = SubTotal;
        this.Disco = Disco;
        this.Venta = Venta;
        this.currentCarrito = currentCarrito;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodigoVenta() {
        return CodigoVenta;
    }

    public void setCodigoVenta(String CodigoVenta) {
        this.CodigoVenta = CodigoVenta;
    }

    public String getCodigoDisco() {
        return codigoDisco;
    }

    public void setCodigoDisco(String codigoDisco) {
        this.codigoDisco = codigoDisco;
    }

    public String getNombreDisco() {
        return NombreDisco;
    }

    public void setNombreDisco(String NombreDisco) {
        this.NombreDisco = NombreDisco;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }

    public double getCantidad() {
        return Cantidad;
    }

    public void setCantidad(double Cantidad) {
        this.Cantidad = Cantidad;
    }

    public double getDescuento() {
        return Descuento;
    }

    public void setDescuento(double Descuento) {
        this.Descuento = Descuento;
    }

    public double getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(double SubTotal) {
        this.SubTotal = SubTotal;
    }

    public IDisco getDisco() {
        return Disco;
    }

    public void setDisco(IDisco Disco) {
        this.Disco = Disco;
    }

    public IVenta getVenta() {
        return Venta;
    }

    public void setVenta(IVenta Venta) {
        this.Venta = Venta;
    }

    @Override
    public double actualizarMontoTotal() {
        return currentCarrito.actualizarMontoTotal() + SubTotal;
    }

    
    
}
