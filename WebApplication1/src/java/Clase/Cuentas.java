/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase;

/**
 *
 * @author Jhonatan
 */
public class Cuentas {
    private String CodigoCU;
    private String CodigoU;
    private double saldo;
    private String tipo;

    public Cuentas() {
    }

    public Cuentas(String CodigoCU, String CodigoU, double saldo, String tipo) {
        this.CodigoCU = CodigoCU;
        this.CodigoU = CodigoU;
        this.saldo = saldo;
        this.tipo = tipo;
    }

    public String getCodigoCU() {
        return CodigoCU;
    }

    public void setCodigoCU(String CodigoCU) {
        this.CodigoCU = CodigoCU;
    }

    public String getCodigoU() {
        return CodigoU;
    }

    public void setCodigoU(String CodigoU) {
        this.CodigoU = CodigoU;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
