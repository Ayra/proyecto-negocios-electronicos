/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase;

import Interface.IDisco;

/**
 *
 * @author Daniel
 */
public class Single implements IDisco{
    private String CodigoD;
    private String GeneroM;
    private String Artista;
    private String Descripcion;
    private double PrecioD;
    private String ImagenD;
    private String EstadoD;

    public Single() {
    }

    public Single(String CodigoD, String GeneroM, String Artista, String Descripcion, double PrecioD, String ImagenD, String EstadoD) {
        this.CodigoD = CodigoD;
        this.GeneroM = GeneroM;
        this.Artista = Artista;
        this.Descripcion = Descripcion;
        this.PrecioD = PrecioD;
        this.ImagenD = ImagenD;
        this.EstadoD = EstadoD;
    }

    public String getCodigoD() {
        return CodigoD;
    }

    public void setCodigoD(String CodigoD) {
        this.CodigoD = CodigoD;
    }

    public String getGeneroM() {
        return GeneroM;
    }

    public void setGeneroM(String GeneroM) {
        this.GeneroM = GeneroM;
    }

    public String getArtista() {
        return Artista;
    }

    public void setArtista(String Artista) {
        this.Artista = Artista;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public double getPrecioD() {
        return PrecioD;
    }

    public void setPrecioD(double PrecioD) {
        this.PrecioD = PrecioD;
    }

    public String getImagenD() {
        return ImagenD;
    }

    public void setImagenD(String ImagenD) {
        this.ImagenD = ImagenD;
    }

    public String getEstadoD() {
        return EstadoD;
    }

    public void setEstadoD(String EstadoD) {
        this.EstadoD = EstadoD;
    }

    @Override
    public String describir_disco() {
        return "Es un single";
    }
}
