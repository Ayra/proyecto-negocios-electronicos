/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.*;
import Clase.DAO.*;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public class CuentasDAOImpl implements CuentasDAO{
    
    public static final CuentasDAOImpl instance = new CuentasDAOImpl();

    private CuentasDAOImpl() {
    
    }
    public static CuentasDAOImpl getInstance(){
        return instance;
    }
    
    @Override
    public boolean ActualizarSaldo(double gasto,Usuario u,String s) {
        boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MODIFICAR_SALDO_CUENTA (?,?,?)");
            cs.setString(1, u.getCodigoUsuario());            
            cs.setDouble(2, gasto);
            cs.setString(3, s);

            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;    
    }

    @Override
    public ArrayList<Cuentas> VerCuentas(String CodigoCli) {
        ArrayList<Cuentas> lista = new ArrayList<Cuentas>();
           Connection cn;
           cn = Conexion.getInstance();
            
            CallableStatement cs = null;
            ResultSet rs = null;
            
            try{
                cs=cn.prepareCall("CALL MOSTRAR_CUENTA (?)");
                cs.setString(1, CodigoCli); 
                rs=cs.executeQuery();
                while(rs.next()){
                    Cuentas p = new Cuentas(rs.getString("Codigo_CU"), rs.getString("Codigo_U"), Double.parseDouble(rs.getString("Saldo")), rs.getString("Tipo"));
                    lista.add(p);
                }
                
            }catch(Exception e){
                System.out.println(e);
            }
            
        return lista;
    }
    
}
