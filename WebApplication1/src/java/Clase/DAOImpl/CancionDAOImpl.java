/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.Cancion;
import Clase.DAO.*;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public class CancionDAOImpl implements CancionDAO{
    public static final CancionDAOImpl instance = new CancionDAOImpl();

    private CancionDAOImpl() {
    }
    
    public static CancionDAOImpl getInstance(){
    return instance;       
    }
    
    @Override
    public ArrayList<Cancion> obtenerCanciones(String Codigo_D) {
       ArrayList<Cancion> lista = new ArrayList<Cancion>();
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_CANCIONES_DISCO(?)");
            cs.setString(1, Codigo_D);
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Cancion dep = new Cancion(rs.getString("Codigo_CA"), rs.getString("Codigo_D"),rs.getInt("Numero_pista"),rs.getString("Nombre"),rs.getString("Duracion"));
                lista.add(dep);
            }
        }catch(Exception e){ 
            System.out.println(e);
        }
        return lista;
    }

    @Override
    public String obtenerNombreDisco(String Codigo_D) {
        Connection cn;
        cn = Conexion.getInstance();
        String nombre="";
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_NOMBRE_DISCO(?)");
            cs.setString(1, Codigo_D);
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                nombre = rs.getString("Descripcion_D");
            }
        }catch(Exception e){ 
            System.out.println(e);
        }
        return nombre;
    }
}
