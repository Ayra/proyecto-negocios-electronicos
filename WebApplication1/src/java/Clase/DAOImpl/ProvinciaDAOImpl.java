/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.DAO.*;
import Clase.Provincia;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jhonatan
 */
public class ProvinciaDAOImpl implements ProvinciaDAO {
    
    public static final ProvinciaDAOImpl instance = new ProvinciaDAOImpl();

    private ProvinciaDAOImpl() {
    }
    
    public static ProvinciaDAOImpl getInstance(){
        return instance;
    }

    @Override
    public List<Provincia> ListarProvinciaPorCodigo(String CodigoDepart) {
        List<Provincia> lista = new ArrayList<Provincia>();
        
        Provincia prov = new Provincia();
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_PROVINCIA_POR_CODIGO (?)");
            cs.setString(1, CodigoDepart);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()){
                prov.setCodigoProv(rs.getInt("IdProvincia"));
                prov.setNombreProv(rs.getString("provincia")); 
                lista.add(prov);
            }
        }catch(Exception e){ 
            System.out.println(e);
        }
        return lista;
    }
    
}
