/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.DAO.*;
import Clase.DetalleVenta2;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public class DetalleVentaDAOImpl implements DetalleVentaDAO {
    
    public static final DetalleVentaDAOImpl instance = new DetalleVentaDAOImpl();

    private DetalleVentaDAOImpl() {
    }
    public static DetalleVentaDAOImpl getInstance(){
        return instance;
    }
    

    @Override
    public ArrayList<DetalleVenta2> obtenerDetalleVenta(String CodigoVenta) {
        ArrayList<DetalleVenta2> lista = new ArrayList<DetalleVenta2>();
        
       Connection cn;
        cn = Conexion.getInstance();
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_DETALLE_VENTA_POR_CODIGO (?)");
            cs.setString(1, CodigoVenta);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()){
                DetalleVenta2 dv = new DetalleVenta2();
                dv.setCodigoVenta(rs.getString("Codigo_V"));
                dv.setNombreDisco(rs.getString("Disco"));
                dv.setPrecio(rs.getDouble("Precio"));
                dv.setCantidad(rs.getDouble("Cantidad"));
                dv.setDescuento(rs.getDouble("Descuento"));
                dv.setSubTotal(rs.getDouble("SubTotal")); 
                lista.add(dv);
            }
        }catch(Exception e){System.out.println(e);}
        return lista;
        
    }
    
}
