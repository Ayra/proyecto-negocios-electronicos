/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.Artista;
import Clase.DAO.ArtistaDAO;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public class ArtistaDAOImpl implements ArtistaDAO{

    private static final ArtistaDAOImpl instance = new ArtistaDAOImpl();
    
    private ArtistaDAOImpl() {
    }
    public static ArtistaDAOImpl getInstance(){
        return instance;
    }

    @Override
    public ArrayList<Artista> obtenerAHabilitados() {
        ArrayList<Artista> lista = new ArrayList<Artista>();
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_ARTISTA_HABILITADOS");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Artista MP = new Artista(rs.getString("Codigo_A"), rs.getString("Nombre_A"), rs.getString("Estado_A"));
                lista.add(MP);
            }
            
        }catch(Exception e){
            System.out.println(e);
        }
        return lista;
    }

    @Override
    public ArrayList<Artista> obtenerAInhabilitados() {
        
        ArrayList<Artista> lista = new ArrayList<Artista>();
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_ARTISTA_INHABILITADOS");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Artista MP = new Artista(rs.getString("Codigo_A"), rs.getString("Nombre_A"), rs.getString("Estado_A"));
                lista.add(MP);
            }
            
        }catch(Exception e){
            System.out.println(e);
        }
        return lista;
    }

    @Override
    public boolean insertarArtista(Artista mp) {
         boolean resp = false;
        
       Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL REGISTRAR_ARTISTA (?)");
            cs.setString(1, mp.getNombreA());
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
        
    }

    @Override
    public boolean actualizarArtista(Artista mp) {
         boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MODIFICAR_ARTISTA (?,?)");
            cs.setString(1, mp.getCodigoA());
            cs.setString(2, mp.getNombreA()); 
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean eliminarArtista(Artista mp) {
         boolean resp = false;
        
       Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL ELIMINAR_ARTISTA (?)");
            cs.setString(1, mp.getCodigoA());
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean recuperarArtista(Artista mp) {
        boolean resp = false;
        
       Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL RECUPERAR_ARTISTA (?)");
            cs.setString(1, mp.getCodigoA());
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public String CodigoArtista() {
        String Codigo = "";
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_CODIGO_ARTISTA");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Codigo = rs.getString("Codigo_A");
            }
        }catch(Exception e){
           System.out.println(e);
        }
        return Codigo;
    }
    
}
