/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.DAO.*;
import Clase.Venta;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public class VentaDAOImpl implements VentaDAO {
    
    public static final VentaDAOImpl instance = new VentaDAOImpl();

    private VentaDAOImpl() {
    }
    public static VentaDAOImpl getInstance(){
        return instance;
    }

    @Override
    public boolean insertarVenta(Venta v) {
        boolean resp = false;
        Connection cn;
        cn = Conexion.getInstance();
        try{
            CallableStatement cs = cn.prepareCall("CALL REGISTRAR_VENTA (?,?)");
            cs.setString(1, v.getCliente());
            cs.setDouble(2, v.getTotal());
            int i = cs.executeUpdate();
            
            if(i == 1){
                resp = true;
            }else{
                resp = false;
            }
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public ArrayList<Venta> ObtenerVentas() {
        ArrayList<Venta> lista = new ArrayList<Venta>();
       Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_VENTA");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Venta v = new Venta();
                v.setCodigoVenta(rs.getString("Codigo_V"));
                v.setCodigoCliente(rs.getString("Codigo_U")); 
                v.setCliente(rs.getString("CLIENTE"));
                v.setTotal(rs.getDouble("Total")); 
                v.setFecha(rs.getString("Fecha")); 
                lista.add(v);
            } 
        }catch(Exception e){
            System.out.println(e);
        }
        return lista;
    }

    @Override
    public boolean eliminarVenta(Venta v) {
        boolean resp = false;
        Connection cn;
        cn = Conexion.getInstance(); 
        
        try{
            CallableStatement cs = cn.prepareCall("CALL ELIMINAR_VENTA (?)");
            cs.setString(1, v.getCodigoVenta());
            int i = cs.executeUpdate();
            
            if(i == 1){
                resp = true;
            }else{
                resp = false;
            }
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public String CodigoVenta() {
        String Codigo = "";
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_CODIGO_VENTA");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Codigo = rs.getString("Codigo_V");
            }
        }catch(Exception e){
           System.out.println(e);
        }
        return Codigo;
    }
    
}
