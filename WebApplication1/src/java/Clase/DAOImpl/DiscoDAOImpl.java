/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.DAO.*;
import Clase.Disco;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public class DiscoDAOImpl implements DiscoDAO {
    
    public static final DiscoDAOImpl instance = new DiscoDAOImpl();

    private DiscoDAOImpl() {
    }
    public static DiscoDAOImpl getInstance(){
        return instance;
    }
    

    @Override
    public ArrayList<Disco> obtenerDiscosHabilitados() {
        ArrayList<Disco> lista = new ArrayList<Disco>();
            Connection cn;
        cn = Conexion.getInstance();
            
            CallableStatement cs = null;
            ResultSet rs = null;
            
            try{
                cs=cn.prepareCall("CALL MOSTRAR_DISCOS_HABILITADOS");
                rs=cs.executeQuery();
                while(rs.next()){
                    Disco p = new Disco(rs.getString("Codigo_D"), rs.getString("Genero_M"), rs.getString("Artista"), rs.getString("Descripcion_D"), rs.getDouble("Precio_D"), rs.getString("Imagen_D"), rs.getString("Estado_D"), rs.getDouble("Stock_D"));
                    lista.add(p);
                }
                
            }catch(Exception e){
                System.out.println(e);
            }
            
        return lista;
    }

    @Override
    public ArrayList<Disco> obtenerDiscosInhabilitados() {
        ArrayList<Disco> lista = new ArrayList<Disco>();
           Connection cn;
           cn = Conexion.getInstance();
            
            CallableStatement cs = null;
            ResultSet rs = null;
            
            try{
                cs=cn.prepareCall("CALL MOSTRAR_DISCOS_ELIMINADOS");
                rs=cs.executeQuery();
                while(rs.next()){
                    Disco p = new Disco(rs.getString("Codigo_D"), rs.getString("Genero_M"), rs.getString("Artista"), rs.getString("Descripcion_D"), rs.getDouble("Precio_D"), rs.getString("Imagen_D"), rs.getString("Estado_D"), rs.getDouble("Stock_D"));
                    lista.add(p);
                }
                
            }catch(Exception e){
                System.out.println(e);
            }
            
        return lista;
    }

    @Override
    public boolean insertarDisco(Disco p) {
        boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL REGISTRAR_DISCO (?,?,?,?,?,?)");
            cs.setString(1, p.getGeneroM());
            cs.setString(2, p.getArtista());
            cs.setString(3, p.getDescripcion());
            cs.setDouble(4, p.getPrecioD());
            cs.setString(5, p.getImagenD());
            cs.setDouble(6, p.getStockD());

            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean actualizarDisco(Disco p) {
        boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MODIFICAR_DISCO (?,?,?,?,?,?,?)");
            cs.setString(1, p.getCodigoD());
            cs.setString(2, p.getGeneroM());
            cs.setString(3, p.getArtista());
            cs.setString(4, p.getDescripcion());
            cs.setDouble(5, p.getPrecioD());
            cs.setString(6, p.getImagenD());
            cs.setDouble(7, p.getStockD());            
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean eliminarDisco(Disco p) {
         boolean resp = false;
        
      Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL ELIMINAR_DISCO (?)");
            cs.setString(1, p.getCodigoD());

            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean recuperarDisco(Disco p) {
        boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL RECUPERAR_DISCO (?)");
            cs.setString(1, p.getCodigoD());
            
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public Disco listarDiscoPorCodigo(String Codigo) {
        Disco p = new Disco();
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL BUSCAR_DISCO_CODIGO (?)");
            cs.setString(1, Codigo);
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                p.setCodigoD(rs.getString("Codigo_D"));
                p.setGeneroM(rs.getString("Nombre_GM"));
                p.setArtista(rs.getString("Nombre_A"));
                p.setDescripcion(rs.getString("Descripcion_D"));
                p.setPrecioD(rs.getDouble("Precio_D"));
                p.setImagenD(rs.getString("Imagen_D")); 
                p.setStockD(rs.getDouble("Stock_D"));
            }
        }catch(Exception e){System.out.println(e);}
        return p;
    }

    @Override
    public String CodigoDisco() {
        String Codigo = "";
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_CODIGO_DISCO");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Codigo = rs.getString("Codigo_D");
            }
        }catch(Exception e){
           System.out.println(e);
        }
        return Codigo;
    }
    
}
