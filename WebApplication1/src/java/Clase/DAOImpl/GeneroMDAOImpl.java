/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.DAO.*;
import Clase.GeneroM;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public class GeneroMDAOImpl implements GeneroMDAO{
    
    public static final GeneroMDAOImpl instance = new GeneroMDAOImpl();

    private GeneroMDAOImpl() {
    }
    public static GeneroMDAOImpl getInstance(){
        return instance;
    }

    @Override
    public ArrayList<GeneroM> ObtenerGMHabilitados() {
        ArrayList<GeneroM> lista = new ArrayList<GeneroM>();
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_GENERO_MUSICAL_HABILITADOS");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                GeneroM cp = new GeneroM(rs.getString("Codigo_GM"), rs.getString("Nombre_GM"), rs.getString("Estado_GM"));
                lista.add(cp);
            }
        }catch(Exception e){ 
            System.out.println(e);
        }
        return lista;
    }

    @Override
    public ArrayList<GeneroM> obtenerGMInhabilitados() {
        ArrayList<GeneroM> lista = new ArrayList<GeneroM>();
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_GENERO_MUSICAL_INHABILITADOS");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                GeneroM CP = new GeneroM(rs.getString("Codigo_GM"), rs.getString("Nombre_GM"), rs.getString("Estado_GM"));
                lista.add(CP);
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return lista;
    }

    @Override
    public boolean insertarGeneroMusical(GeneroM cp) {
         boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL REGISTRAR_GENERO_MUSICAL (?)");
            cs.setString(1, cp.getNombreGM());
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean actualizarGeneroMusical(GeneroM cp) {
        boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MODIFICAR_GENERO_MUSICAL (?,?)");
            cs.setString(1, cp.getCodigoGM());
            cs.setString(2, cp.getNombreGM()); 
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean darBajaGeneroMusical(GeneroM cp) {
        boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL ELIMINAR_GENERO_MUSICAL (?)");
            cs.setString(1, cp.getCodigoGM()); 
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public boolean darAltaGeneroMusical(GeneroM cp) {
        boolean resp = false;
        
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL RECUPERAR_GENERO_MUSICAL (?)");
            cs.setString(1, cp.getCodigoGM()); 
            int i = cs.executeUpdate();
            
            if(i==1)
                resp = true;
            else
                resp = false;
            
        }catch(Exception e){System.out.println(e);}
        return resp;
    }

    @Override
    public String CodigoGeneroM() {
        String Codigo = "";
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_CODIGO_GENERO_MUSICAL");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Codigo = rs.getString("Codigo_GM");
            }
        }catch(Exception e){
           System.out.println(e);
        }
        return Codigo;
    }
    
}
