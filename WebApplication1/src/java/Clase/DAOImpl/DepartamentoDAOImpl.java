/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAOImpl;

import Clase.DAO.*;
import Clase.Departamento;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jhonatan
 */
public class DepartamentoDAOImpl implements DepartamentoDAO{
    
    public static final DepartamentoDAOImpl instance = new DepartamentoDAOImpl();

    private DepartamentoDAOImpl() {
    }
    public static DepartamentoDAOImpl getInstance(){
    return instance;       
    }
    

    @Override
    public List<Departamento> ObtenerDepartamentos() {
        List<Departamento> lista = new ArrayList<Departamento>();
        Connection cn;
        cn = Conexion.getInstance();
        
        try{
            CallableStatement cs = cn.prepareCall("CALL MOSTRAR_DEPARTAMENTOS");
            ResultSet rs = cs.executeQuery();
            while(rs.next()){
                Departamento dep = new Departamento(rs.getInt("IdDepartamento"), rs.getString("Departamento"));
                lista.add(dep);
            }
        }catch(Exception e){ 
            System.out.println(e);
        }
        return lista;
    }
    
}
