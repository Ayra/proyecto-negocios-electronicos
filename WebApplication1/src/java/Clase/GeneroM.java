/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase;

/**
 *
 * @author Daniel
 */
public class GeneroM {
    private String CodigoGM;
    private String NombreGM;
    private String EstadoGM;

    public GeneroM() {
    }

    public GeneroM(String CodigoGM, String NombreGM, String EstadoGM) {
        this.CodigoGM = CodigoGM;
        this.NombreGM = NombreGM;
        this.EstadoGM = EstadoGM;
    }

    public String getCodigoGM() {
        return CodigoGM;
    }

    public void setCodigoGM(String CodigoGM) {
        this.CodigoGM = CodigoGM;
    }

    public String getNombreGM() {
        return NombreGM;
    }

    public void setNombreGM(String NombreGM) {
        this.NombreGM = NombreGM;
    }

    public String getEstadoGM() {
        return EstadoGM;
    }

    public void setEstadoGM(String EstadoGM) {
        this.EstadoGM = EstadoGM;
    }
}
