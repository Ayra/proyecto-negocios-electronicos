/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.Usuario;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface UsuarioDAO {
    
    public Usuario VerificarUsuario(String id_usuario);
    public ArrayList<Usuario> MostrarUsuarioHabilitado();
    public ArrayList<Usuario> MostrarUsuarioInhabilitado();
    public boolean RegistrarUsuario(Usuario usu);
    public boolean ModificarUsuario(Usuario usu);
    public boolean ModificarClaveUsuario(Usuario usu);
    public boolean ModificarEmailUsuario(Usuario usu);
    public boolean DarDeBajaUsuario(Usuario usu);
    public boolean DarDeAltaUsuario(Usuario usu);
    public Usuario listarUsuarioPorCodigo(String codigo);
}
