/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.Disco;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface DiscoDAO {
    public ArrayList<Disco> obtenerDiscosHabilitados();
    public ArrayList<Disco> obtenerDiscosInhabilitados();
    public boolean insertarDisco(Disco p);
    public boolean actualizarDisco(Disco p);
    public boolean eliminarDisco(Disco p);
    public boolean recuperarDisco(Disco p);
    public Disco listarDiscoPorCodigo(String Codigo);
    public String CodigoDisco();
}
