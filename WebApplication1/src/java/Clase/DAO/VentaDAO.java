/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.Venta;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface VentaDAO {
    public boolean insertarVenta(Venta v);
    public ArrayList<Venta> ObtenerVentas();
    public boolean eliminarVenta(Venta v);
    public String CodigoVenta();
}
