/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;


import Clase.Artista;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface ArtistaDAO {
    
    public ArrayList<Artista> obtenerAHabilitados();
    public ArrayList<Artista> obtenerAInhabilitados();
    public boolean insertarArtista (Artista mp);
    public boolean actualizarArtista (Artista mp);
    public boolean eliminarArtista (Artista mp);
    public boolean recuperarArtista (Artista mp);
    public String CodigoArtista();
    
}
