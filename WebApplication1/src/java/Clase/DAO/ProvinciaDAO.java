/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.Provincia;
import java.util.List;

/**
 *
 * @author Jhonatan
 */
public interface ProvinciaDAO {
    public List<Provincia> ListarProvinciaPorCodigo(String CodigoDepart);
    
}
