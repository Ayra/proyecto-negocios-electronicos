/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.Cancion;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface CancionDAO {
    public ArrayList<Cancion> obtenerCanciones(String Codigo_D);
    public String obtenerNombreDisco(String Codigo_D);
}
