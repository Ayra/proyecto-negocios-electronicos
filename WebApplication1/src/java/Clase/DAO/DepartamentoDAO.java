/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.Departamento;
import java.util.List;

/**
 *
 * @author Jhonatan
 */
public interface DepartamentoDAO {
    
     public List<Departamento> ObtenerDepartamentos();
}
