/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.GeneroM;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface GeneroMDAO {
    
    public ArrayList<GeneroM> ObtenerGMHabilitados();
    public ArrayList<GeneroM> obtenerGMInhabilitados();
    public boolean insertarGeneroMusical (GeneroM cp);
    public boolean actualizarGeneroMusical (GeneroM cp);
    public boolean darBajaGeneroMusical (GeneroM cp);
    public boolean darAltaGeneroMusical (GeneroM cp);
    public String CodigoGeneroM();
}
