/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.DetalleVenta2;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface DetalleVentaDAO {
    
    public ArrayList<DetalleVenta2> obtenerDetalleVenta(String CodigoVenta);
}
