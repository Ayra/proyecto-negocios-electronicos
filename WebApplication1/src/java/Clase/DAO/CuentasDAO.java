/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase.DAO;

import Clase.*;
import java.util.ArrayList;

/**
 *
 * @author Jhonatan
 */
public interface CuentasDAO {
    public boolean ActualizarSaldo(double gasto,Usuario u,String s);
    public ArrayList<Cuentas> VerCuentas(String CodigoCli);
}
