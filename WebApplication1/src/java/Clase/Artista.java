/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase;
/**
 *
 * @author Daniel
 */
public class Artista {
    private String CodigoA;
    private String NombreA;
    private String EstadoA;

    public Artista() {
    }

    public Artista(String CodigoA, String NombreA, String EstadoA) {
        this.CodigoA = CodigoA;
        this.NombreA = NombreA;
        this.EstadoA = EstadoA;
    }

    public String getCodigoA() {
        return CodigoA;
    }

    public void setCodigoA(String CodigoA) {
        this.CodigoA = CodigoA;
    }

    public String getNombreA() {
        return NombreA;
    }

    public void setNombreA(String NombreA) {
        this.NombreA = NombreA;
    }

    public String getEstadoA() {
        return EstadoA;
    }

    public void setEstadoA(String EstadoA) {
        this.EstadoA = EstadoA;
    }
    
    
}
