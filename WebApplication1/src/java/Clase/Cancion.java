/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clase;

/**
 *
 * @author Jhonatan
 */
public class Cancion {
    private String CodigoCA;
    private String CodigoD;
    private int numero_pista;
    private String nombre;
    private String duracion;

    public Cancion() {
    }

    public Cancion(String CodigoCA, String CodigoD, int numero_pista, String nombre, String duracion) {
        this.CodigoCA = CodigoCA;
        this.CodigoD = CodigoD;
        this.numero_pista = numero_pista;
        this.nombre = nombre;
        this.duracion = duracion;
    }

    public String getCodigoCA() {
        return CodigoCA;
    }

    public void setCodigoCA(String CodigoCA) {
        this.CodigoCA = CodigoCA;
    }

    public String getCodigoD() {
        return CodigoD;
    }

    public void setCodigoD(String CodigoD) {
        this.CodigoD = CodigoD;
    }

    public int getNumero_pista() {
        return numero_pista;
    }

    public void setNumero_pista(int numero_pista) {
        this.numero_pista = numero_pista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }
    
    
}
