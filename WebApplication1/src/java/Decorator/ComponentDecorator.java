/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Decorator;

/**
 *
 * @author Aula
 */
public abstract class ComponentDecorator extends CarritoCompra {

        @Override
        public abstract double actualizarMontoTotal();
    
}
