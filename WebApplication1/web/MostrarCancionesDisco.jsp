<%-- 
    Document   : MostrarCP
    Created on : 04-oct-2015, 21:11:09
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.CancionDAOImpl"%>
<%@page import="Clase.DAOImpl.GeneroMDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Clase.*" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>
    
    <body>
        <%  String Codigo_D= request.getParameter("codigoD");
            
        %>
        <form id="frmMostrarCabeceraGM">
            <table id="tablaMostrarCabeceraGM">
                <tr>
                    <th>
                        <h1> Canciones de <%= CancionDAOImpl.getInstance().obtenerNombreDisco(Codigo_D)%> </h1>
                    </th>
                </tr>
                <tr>
                    <td>
                        <a href="Catalogo.jsp" class="link">Regresar al Catalogo</a>
                    </td>
                </tr>
            </table>
        </form>
        
        <hr>
        
        <form id="frmMostrarCuerpoGM">
            <table id="tablaMostrarCuerpoGM">
                <tr>
                    <th class="Titulo">
                        NUMERO DE PISTA 
                    </th>
                    <th class="Titulo">
                        NOMBRE
                    </th>
                    <th class="Titulo">
                        DURACION
                    </th>
               
                </tr>    
                
                <%
                    ArrayList<Cancion> lista = CancionDAOImpl.getInstance().obtenerCanciones(Codigo_D);
                    for(int i=0; i<lista.size(); i++){
                        Cancion C = lista.get(i);
                %>
                        
                <tr>
                    <td>
                        <%=C.getNumero_pista()%>
                    </td>
                    <td>
                        <%=C.getNombre()%>
                    </td>
                    <td>
                        <%=C.getDuracion()%>
                    </td>
                   
                </tr>
                
                <%
                    }
                %>

            </table>
        </form>

    </body>
    
    
</html>
