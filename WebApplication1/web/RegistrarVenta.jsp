<%-- 
    Document   : Venta
    Created on : 28-oct-2015, 16:51:48
    Author     : Daniel
--%>

<%@page import="Decorator.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="Clase.*" %>
<%@page import="java.text.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/RegistrarVenta.js"></script>
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>
    <body>
        
        <%
            String codigoVenta = ObtenerCodigo.CodigoVenta();
            String cliente = (String)session.getAttribute("parametroCliente");
        %>
        
        <form action="Servlet_Venta" method="post" id="frmRegistrarVenta">
            <br>
            <table id="tablaRegistrarVenta">
                <tr>
                    <th colspan="7" class="TituloDV">
                        <h3>CARRITO DE COMPRAS</h3>
                        <input type="hidden" name="txtCodigoV" value="<%=codigoVenta %>">
                    </th>
                </tr>
                <tr>
                    <th colspan="2" class="TituloDV">
                        CLIENTE :  
                    </th>
                    <td colspan="5" class="Contenido">
                        <%=cliente %>
                        <input type="hidden" name="txtCliente" value="<%=cliente %>" size="50" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="FilaEnBlanco">
                        <br>
                    </td>
                </tr>
                <tr>
                    <th class="TituloDV">
                        N°
                    </th>
                    <th class="TituloDV">
                        DISCO 
                    </th>
                    <th class="TituloDV">
                        P/U (S/.)
                    </th>
                    <th class="TituloDV">
                        CANTIDAD 
                    </th>
                    <th class="TituloDV">
                        DESC (S/.)
                    </th>
                    <th class="TituloDV">
                        SUBTOTAL (S/.)
                    </th>
                    <th class="TituloDV">
                        OPCION
                    </th>
                </tr>
                
                <%
                    DecimalFormat df = new DecimalFormat("0.00");
                    DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
                    dfs.setDecimalSeparator('.');
                    df.setDecimalFormatSymbols(dfs);
                    
                    CarritoCompra carrito = new BaseCarritoCompra();
                    
                    //double total = 0;
                    ArrayList<DetalleVenta2> lista = (ArrayList<DetalleVenta2>)session.getAttribute("carrito");
                    
                    if(lista != null){
                        for(DetalleVenta2 dv:lista){
                            %>
                            
                            <tr>
                                <td class="Contenido">
                                    <%=dv.getNumero() %>
                                </td>
                                <td class="Contenido">
                                    <%=dv.getNombreDisco()%>
                                    <input type="hidden" name="nombreDisco" value="<%=dv.getNombreDisco()%>">
                                </td>
                                <td class="Contenido">
                                    <%=df.format(dv.getPrecio())%>
                                    <input type="hidden" name="precioDisco" value="<%=dv.getPrecio()%>">
                                </td>
                                <td class="Contenido">
                                    <%=dv.getCantidad()%>
                                    <input type="hidden" name="cantidadDisco" value="<%=dv.getCantidad()%>">
                                </td>
                                <td class="Contenido">
                                    <%=df.format(dv.getDescuento())%>
                                    <input type="hidden" name="descuentoDisco" value="<%=dv.getDescuento()%>">
                                </td>
                                <td class="Contenido">
                                    <div> <%=df.format(dv.getSubTotal()) %> </div>
                                    <input type="hidden" name="subTotalDisco" value="<%=dv.getSubTotal()%>">
                                </td> 
                                <td whidh="100" class="Opcion">
                                    <input type="button" name="btnEliminar" id="btnEliminar" class="button" value="Eliminar" onclick="location.href='Servlet_Venta?numero=<%=dv.getNumero() %>&&accion=quitar'">
                                </td>
                            </tr>
                            <%
                                
                            carrito = new DetalleVenta2(dv.getNumero(),dv.getCodigoVenta(),dv.getCodigoDisco(),dv.getNombreDisco(),dv.getPrecio(),dv.getCantidad(),dv.getDescuento(),dv.getSubTotal(),dv.getDisco(),dv.getVenta(),carrito);    
                            
                        }
                    }
                %>
                <tr>
                    <th colspan="5" class="TituloDV">
                        <div> TOTAL (S/.)  </div>
                    </th>
                    <th class="Contenido">
                        <div> <%=String.valueOf(df.format(carrito.actualizarMontoTotal())) %> </div>
                        <input type="hidden" name="txtTotal" value="<%=String.valueOf(df.format(carrito.actualizarMontoTotal())) %>" readonly="readonly">
                    </th>
                    <th class="FilaEnBlanco">
                    </th>
                </tr>
                <tr>
                    <td colspan="7" class="FilaEnBlanco">
                        <br>
                    </td>
                </tr>
            </table>
                    <div id="modopago">
                        <br/><br/>
                        <label for="lbl">Modo de Pago:  </label>
                        <select name="ModoPago">
                        <option value="MasterCard" selected>MasterCard</option> 
                        <option value="Paypal">Paypal</option>
                        <option value="Visa">Visa</option>
                        </select>

                    </div>>
            
            <table id="tablaRegresar">
                <tr>
                    <td>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" name="btnRegistrar" id="btnRegistrarVenta" class="button" value="Registrar compra">
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                    </td>
                </tr>
            </table>        
                    <input type="hidden" name="accion" value="RegistrarVenta"><br>
                    
        </form>
                    <h3 align="center">
                        <a href="Catalogo.jsp" class="link">Seguir comprando</a>
                    </h3>
    </body>
</html>
