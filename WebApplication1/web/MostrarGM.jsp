<%-- 
    Document   : MostrarCP
    Created on : 04-oct-2015, 21:11:09
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.GeneroMDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Clase.*" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>
    
    <body>
        <form id="frmMostrarCabeceraGM">
            <table id="tablaMostrarCabeceraGM">
                <tr>
                    <th>
                        <h1> Generos musicales habilitados </h1>
                    </th>
                </tr>
                <tr>
                    <td>
                        <a href="CuentaAdministrador.jsp" class="link">Pagina principal</a>
                    </td>
                </tr>
            </table>
        </form>
        
        <hr>
        
        <form id="frmMostrarCuerpoGM">
            <table id="tablaMostrarCuerpoGM">
                <tr>
                    <th class="Titulo">
                        CODIGO
                    </th>
                    <th class="Titulo">
                        NOMBRE
                    </th>
                    <th class="Titulo">
                        ESTADO
                    </th>
                    <th colspan="2" class="Titulo">
                        OPCION
                    </th>
                </tr>    
                
                <%
                    ArrayList<GeneroM> lista = GeneroMDAOImpl.getInstance().ObtenerGMHabilitados();
                    for(int i=0; i<lista.size(); i++){
                        GeneroM GM = lista.get(i);
                        %>
                        
                <tr>
                    <td>
                        <%=GM.getCodigoGM() %>
                    </td>
                    <td>
                        <%=GM.getNombreGM() %>
                    </td>
                    <td>
                        <%=GM.getEstadoGM() %>
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnModificar" value="Modificar" id="btnModificar" class="button" onclick="location.href='ModificarGM.jsp?codigoGM=<%=GM.getCodigoGM() %>&&nombreGM=<%=GM.getNombreGM() %>&&estadoGM=<%=GM.getEstadoGM() %>'">
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnEliminar" value="Eliminar" id="btnEliminar" class="button" onclick="location.href='Servlet_GM?codigoGM=<%=GM.getCodigoGM()%>&&accion=eliminar'">
                    </td>
                </tr>
                
                <%
                    }
                %>

            </table>
        </form>

    </body>
    
    
</html>
