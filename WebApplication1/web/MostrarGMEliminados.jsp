<%-- 
    Document   : MostrarCPEliminados
    Created on : 04-oct-2015, 23:53:44
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.GeneroMDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Clase.*" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>

    <body>
        
        <form id="frmMostrarCabeceraGMEliminados">
            <table id="tablaMostrarCabeceraGMEliminados">
                <tr>
                    <th>
                        <h1> Generos musicales eliminados </h1>
                    </th>
                </tr>
                <tr>
                    <td>
                        <a href="CuentaAdministrador.jsp" class="link">Pagina principal</a>
                    </td>
                </tr>
            </table>
        </form>
        
        <hr>    
        
        <form id="frmMostrarCuerpoGMEliminados">
            <table id="tablaMostrarCuerpoGMEliminados">
                <tr>
                    <th class="Titulo">
                        CODIGO
                    </th>
                    <th class="Titulo">
                        NOMBRE
                    </th>
                    <th class="Titulo">
                        ESTADO
                    </th>
                    <th class="Titulo">
                        OPCION
                    </th>
                </tr>
                
                <%
                    ArrayList<GeneroM> lista = GeneroMDAOImpl.getInstance().obtenerGMInhabilitados();
                    for(int i=0; i<lista.size(); i++){
                        GeneroM GM = lista.get(i);
                %>
                    
                <tr>
                    <td>
                        <%=GM.getCodigoGM() %>
                    </td>
                    <td>
                        <%=GM.getNombreGM() %>
                    </td>
                    <td>
                        <%=GM.getEstadoGM() %>
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnRecuperar" id="btnRecuperar" class="button" value="Recuperar" onclick="location.href='Servlet_GM?codigoGM=<%=GM.getCodigoGM() %>&&accion=recuperar'">
                    </td>
                </tr>
                
                <%
                    }
                %>
            </table>
        </form> 
    </body>
</html>
