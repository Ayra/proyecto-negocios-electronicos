<%-- 
    Document   : MostrarProducto
    Created on : 08-oct-2015, 13:50:45
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.DiscoDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="Clase.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>
    
    <body>
        
        <form id="frmMostrarCabeceraDisco">
            <table id="tablaMostrarCabeceraDisco">
                <tr>
                    <th>
                        <h1> Discos habilitados </h1>
                    </th>
                </tr>
                <tr>
                    <td>
                        <a href="CuentaAdministrador.jsp" class="link">Pagina principal</a>
                    </td>
                </tr>
            </table>
        </form>
        
        <hr>
        
        <form id="frmMostrarCuerpoDisco">
            <table id="tablaMostrarCuerpoDisco">
                <tr>
                    <th class="Titulo">
                        CODIGO
                    </th>
                    <th class="Titulo">
                        GENERO MUSICAL
                    </th>
                    <th class="Titulo">
                        ARTISTA
                    </th>
                    <th class="Titulo">
                        DESCRIPCION
                    </th>
                    <th class="Titulo">
                        PRECIO
                    </th>
                    <th class="Titulo">
                        IMAGEN
                    </th>
                    <th class="Titulo">
                        STOCK
                    </th>
                    <th class="Titulo">
                        ESTADO
                    </th>
                    <th colspan="2" class="Titulo">
                        OPCION
                    </th>
                </tr>
                
                <%
                    ArrayList<Disco> lista = DiscoDAOImpl.getInstance().obtenerDiscosHabilitados();
                    for(int i=0; i<lista.size(); i++){
                        Disco D = lista.get(i);
                %>
                    
                <tr>
                    <td>
                        
                        <%=D.getCodigoD() %>
                    </td>
                    <td>
                        <%=D.getGeneroM()%>
                    </td>
                    <td>
                        <%=D.getArtista()%>
                    </td>
                    <td>
                        <%=D.getDescripcion() %>
                    </td>
                    <td>
                        <%=D.getPrecioD() %>
                    </td>
                    <td>
                        <%=D.getImagenD() %>
                    </td>
                    <td>
                        <%=D.getStockD() %>
                    </td>
                    <td>
                        <%=D.getEstadoD() %>
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnModificar" id="btnModificar" class="button" value="Modificar" onclick="location.href='ModificarDisco.jsp?codigoD=<%=D.getCodigoD() %>'"> 
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnEliminar" id="btnEliminar" class="button" value="Eliminar" onclick="location.href='Servlet_Disco?codigoD=<%=D.getCodigoD() %>&&accion=eliminar'">
                    </td>
                </tr>
                
                <%
                    }
                %>
    
            </table>
        </form>
    </body>
</html>
