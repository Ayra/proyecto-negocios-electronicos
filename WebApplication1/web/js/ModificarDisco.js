$(document).ready(function(){
    $('#txtModificarImagen').attr('disabled','disabled');
    
    $('#btnCancelar').click(function(){
        history.back();
    });
    
    $('#btnGuardar').click(function(){
        validarDatos();
    });
    
    $('#SelectImagenActual').click(function(){
        $('#txtImagen').removeAttr('disabled');
        $('#txtModificarImagen').attr('disabled','disabled');
    });
    
    $('#SelectModificarImagen').click(function(){
        $('#txtImagen').attr('disabled','disabled');
        $('#txtModificarImagen').removeAttr('disabled');
    });
});

function validarDatos(){
    var nombreGM = $('#txtNombreGM').val();
    var nombreA = $('#txtNombreA').val();
    var descripcionD = $('#txtDescripcionD').val();
    var precioD = $('#txtPrecioD').val();
    var imagenD = $('#txtImagen').val();
    
    if(nombreGM == ""){
        alert("El campo genero musical no puede estar vacio");
        $('#txtNombreCP').focus();
    }
    else if(nombreA == ""){
        alert("El campo artista no puede estar vacio");
        $('#txtNombreMP').focus();
    }
    else if(descripcionD == ""){
        alert("El campo descripcion disco no puede estar vacio");
        $('#txtDescripcionP').focus();
    }
    else if(precioD == ""){
        alert("El campo precio disco no puede estar vacio");
        $('#txtPrecioP').focus();
    }
    else if(imagenD == ""){
        alert("El campo imagen disco no puede estar vacio");
        $('#txtImagen').focus();
    }
    else{
        $('#frmModificarDisco').submit();
    }
}