$(document).ready(function(){
    $('#btnCancelar').click(function(){
        $(location).attr('href',"MostrarGM.jsp");
    });
    
    $('#btnGuardar').click(function(){
        validarDatos();
    });
});

function validarDatos(){
    var nombre = $('#txtNombre').val();
    
    if(nombre == ""){
        alert("El campo genero musical no puede estar vacio");
        $('#txtNombre').focus();
    }
    else{
        $('#frmModificarGM').submit();
    }
}