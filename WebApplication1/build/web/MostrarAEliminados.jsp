<%-- 
    Document   : MostrarMPEliminados
    Created on : 07-oct-2015, 12:37:32
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.ArtistaDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="Clase.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>

    <body>
        
        <form id="frmMostrarCabeceraAEliminados">
            <table id="tablaMostrarCabeceraAEliminados">
                <tr>
                    <th>
                        <h1> Artistas eliminados </h1>
                    </th>
                </tr>
                <tr>
                    <td>
                        <a href="CuentaAdministrador.jsp" class="link"><h4> Pagina principal </h4></a>
                    </td>
                </tr>
            </table>
        </form>
        
        <hr>
        
        <form id="frmMostrarCuerpoAEliminados">
            <table id="tablaMostrarCuerpoAEliminados">
                <tr>
                    <th class="Titulo">
                        CODIGO
                    </th>
                    <th class="Titulo">
                        NOMBRE
                    </th>
                    <th class="Titulo">
                        ESTADO
                    </th>
                    <th class="Titulo">
                        OPCION
                    </th>
                </tr>
                
                <%
                    ArrayList<Artista> lista = ArtistaDAOImpl.getInstance().obtenerAInhabilitados();
                    for(int i=0; i<lista.size(); i++){
                        Artista A = lista.get(i);
                %>
  
                <tr>
                    <td>
                        <%=A.getCodigoA() %>
                    </td>
                    <td>
                        <%=A.getNombreA() %>
                    </td>
                    <td>
                        <%=A.getEstadoA() %>
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnRecuperar" id="btnRecuperar" class="button" value="Recuperar" onclick="location.href='Servlet_A?codigoA=<%=A.getCodigoA() %>&&accion=recuperar'">
                    </td>
                </tr>
                
                <%
                    }
                %>
    
            </table>
        </form>
    </body>
    
</html>
