<%-- 
    Document   : ModificarProducto
    Created on : 08-oct-2015, 20:30:47
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.DiscoDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="Clase.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/ModificarDisco.js"></script>
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>
    
    <%
        Disco d = DiscoDAOImpl.getInstance().listarDiscoPorCodigo(request.getParameter("codigoD"));
    %>
    
    <body onload="cargar()">
        <form name="frm" action="Servlet_Disco" method="post" id="frmModificarDisco">
            <table id="tablaModificarDisco">
                <tr>
                    <th colspan="2">
                        <h1>Modificar disco</h1>
                    </th>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="txtCodigo" value="<%=d.getCodigoD() %>">
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Codigo producto :
                    </td>
                    <td>
                        <dd> <%=d.getCodigoD() %> </dd>
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Nombre Genero musical :
                    </td>
                    <td>
                        <dd> <input type="text" name="txtNombreGM" id="txtNombreGM" value="<%=d.getGeneroM()%>" size="30" maxlength="30"> </dd>
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Nombre Artista :
                    </td>
                    <td>
                        <dd> <input type="text" name="txtNombreA" id="txtNombreA" value="<%=d.getArtista()%>" size="30" maxlength="30"> </dd>
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Descripcion disco :
                    </td>
                    <td>
                        <dd> <input type="text" name="txtDescripcionD" id="txtDescripcionD" value="<%=d.getDescripcion() %>" size="30" maxlength="30"> </dd>
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Precio disco :
                    </td>
                    <td>
                        <dd> <input type="text" name="txtPrecioD" id="txtPrecioD" value="<%=d.getPrecioD() %>" size="30" maxlength="30"> </dd>
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Stock disco :
                    </td>
                    <td>
                        <dd> <input type="text" name="txtStockD" id="txtStockD" value="<%=d.getStockD() %>" size="30" maxlength="30"> </dd>
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Imagen actual : 
                    </td>
                    <td>
                            <dd> <input type="radio" name="selected" value="SelectImagenActual" id="SelectImagenActual" checked="checked"> 
                            <input type="text" name="txtImagen" id="txtImagen" value="<%=d.getImagenD() %>" size="30" maxlength="30"> </dd>
                    </td>
                </tr>
                <tr>
                    <td class="primeraColumna">
                        Modificar imagen : 
                    </td>
                    <td>
                            <dd> <input type="radio" name="selected" value="SelectModificarImagen" id="SelectModificarImagen"> 
                            <input type="file" name="txtModificarImagen" id="txtModificarImagen" size="30"> </dd>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="Botones">
                        <br>
                        <input type="button" name="btnCancelar" id="btnCancelar" class="button" value="Cancelar">
                        <input type="button" name="btnGuardar" id="btnGuardar" class="button" value="Guardar">
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                    </td>
                </tr>
            </table>
                    <input type="hidden" name="accion" value="actualizar">
        </form>
    </body>
</html>
