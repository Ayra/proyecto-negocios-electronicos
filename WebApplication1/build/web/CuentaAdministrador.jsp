<%-- 
    Document   : CuentaAdministrador
    Created on : 29-sep-2015, 22:48:52
    Author     : Daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>
    <body>
        <form id="frmAdministrador">
            <table id="tablaAdministrador">
                <tr>
                    <th colspan="5">
                        <h2> Menu administrador </h2>
                    </th>
                </tr>
                <tr>
                    <th>
                        Registrar nuevo
                    </th>
                    <th>
                        Registros habilitados
                    </th>
                    <th>
                        Registros inhabilitados
                    </th>
                    <th>
                        Pedidos realizados
                    </th>
                    <th>
                        Mi catalogo
                    </th>
                </tr>
                <tr>
                    <td>
                        <a href="RegistrarGeneroMusical.jsp" class="link">Genero musical</a>
                    </td>
                    <td>
                        <a href="MostrarGM.jsp" class="link">Genero musical</a>
                    </td>
                    <td>
                        <a href="MostrarGMEliminados.jsp" class="link">Genero musical</a>
                    </td>
                    <td>
                        <a href="MostrarVenta.jsp" class="link">Ir a ventas</a>
                    </td>
                    <td>
                        <a href="Catalogo.jsp" class="link">Ir a catalogo</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="RegistrarArtista.jsp" class="link">Artista</a>
                    </td>
                    <td>
                        <a href="MostrarA.jsp" class="link">Artista</a>
                    </td>
                    <td>
                        <a href="MostrarAEliminados.jsp" class="link">Artista</a>
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="RegistrarDisco.jsp" class="link">Disco</a>
                    </td>
                    <td>
                        <a href="MostrarDisco.jsp" class="link">Disco</a>
                    </td>
                    <td>
                        <a href="MostrarDiscoEliminados.jsp" class="link">Disco</a>
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                        <a href="MostrarUsuario.jsp" class="link">Usuario</a>
                    </td>
                    <td>
                        <a href="MostrarUsuarioEliminado.jsp" class="link">Usuario</a>
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
        </form>
        
    </body>
</html>
