<%-- 
    Document   : MostrarMP
    Created on : 07-oct-2015, 11:51:18
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.ArtistaDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="Clase.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>

    <body>
        
        <form id="frmMostrarCabeceraA">
            <table id="tablaMostrarCabeceraA">
                <tr>
                    <th>
                        <h1> Artistas habilitados </h1>
                    </th>
                </tr>
                <tr>
                    <td>
                        <a href="CuentaAdministrador.jsp" class="link"><h4> Pagina principal </h4></a>
                    </td>
                </tr>
            </table>
        </form>
        
        <hr>
        
        <form id="frmMostrarCuerpoA">
            <table id="tablaMostrarCuerpoA">
                
                <tr>
                    <th class="Titulo">
                        CODIGO
                    </th>
                    <th class="Titulo">
                        NOMBRE
                    </th>
                    <th class="Titulo">
                        ESTADO
                    </th>
                    <th colspan="2" class="Titulo">
                        OPCION
                    </th>
                </tr>
                
                <%
                    
                    ArrayList<Artista> lista = ArtistaDAOImpl.getInstance().obtenerAHabilitados();
                    for(int i=0; i<lista.size(); i++){
                        Artista A = lista.get(i);
                %>

                <tr>
                    <td>
                        <%=A.getCodigoA() %>
                    </td>
                    <td>
                        <%=A.getNombreA() %>
                    </td>
                    <td>
                        <%=A.getEstadoA() %>
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnModificar" id="btnModificar" class="button" value="Modificar" onclick="location.href='ModificarA.jsp?codigoA=<%=A.getCodigoA() %>&&nombreA=<%=A.getNombreA() %>&&estadoMP=<%=A.getEstadoA() %>'">
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnEliminar" id="btnEliminar" class="button" value="Eliminar" onclick="location.href='Servlet_A?codigoA=<%=A.getCodigoA() %>&&accion=eliminar'">
                    </td>
                </tr>
                
                <%
                    }
                %>

            </table>
        </form>
    </body>
    
</html>
