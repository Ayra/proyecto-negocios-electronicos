<%-- 
    Document   : MostrarProductosEliminados
    Created on : 09-oct-2015, 10:51:49
    Author     : Daniel
--%>

<%@page import="Clase.DAOImpl.DiscoDAOImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="Clase.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="css1.css" />
        <title>JSP Page</title>
    </head>
    
    <body>
        <form id="frmMostrarCabeceraDiscosEliminados">
            <table id="tablaMostrarCabeceraDiscosEliminados">
                <tr>
                    <th>
                        <h1> Disco inhabilitados </h1>
                    </th>   
                </tr>
                <tr>
                    <td>
                        <a href="CuentaAdministrador.jsp" class="link">Pagina principal</a>
                    </td>
                </tr>
            </table>
        </form>
        
        <hr>
        
        <form id="frmMostrarCuerpoDiscosEliminados">
            <table id="tablaMostrarCuerpoDiscosEliminados">
                <tr>
                    <th class="Titulo">
                        CODIGO
                    </th>
                    <th class="Titulo">
                        GENERO MUSICAL
                    </th>
                    <th class="Titulo">
                        ARTISTA
                    </th>
                    <th class="Titulo">
                        DESCRIPCION
                    </th>
                    <th class="Titulo">
                        PRECIO
                    </th>
                    <th class="Titulo">
                        IMAGEN
                    </th>
                    <th class="Titulo">
                        STOCK
                    </th>
                    <th class="Titulo">
                        ESTADO
                    </th>
                    <th class="Titulo">
                        OPCION
                    </th>
                </tr>
                
                <% 
                    ArrayList<Disco> lista = DiscoDAOImpl.getInstance().obtenerDiscosInhabilitados();
                    for(int i=0; i<lista.size(); i++){
                        Disco D = lista.get(i);
                %>
                    
                <tr>
                    <td>
                        <%=D.getCodigoD() %>
                    </td>
                    <td>
                        <%=D.getGeneroM()%>
                    </td>
                    <td>
                        <%=D.getArtista()%>
                    </td>
                    <td>
                        <%=D.getDescripcion() %>
                    </td>
                    <td>
                        <%=D.getPrecioD() %>
                    </td>
                    <td>
                        <%=D.getImagenD() %>
                    </td>
                    <td>
                        <%=D.getStockD() %>
                    </td>
                    <td>
                        <%=D.getEstadoD() %>
                    </td>
                    <td class="Opcion">
                        <input type="button" name="btnRecuperar" id="btnRecuperar" class="button" value="Recuperar" onclick="location.href='Servlet_Disco?codigoD=<%=D.getCodigoD() %>&&accion=recuperar'">
                    </td>
                </tr>
                
                <%
                    }
                %>
    
            </table>
        </form>
    </body>
</html>
