$(document).ready(function (){
    var tipo = $('#txtTipo').val();
    
    if(tipo == "ADMINISTRADOR"){
        cuentaAdministrador()
    }
    else if(tipo == "CLIENTE"){
        cuentaUsuario();
    }
    else{
        cuentaNegada();
        $('.BotonADD').click(function (){
            $(location).attr('href',"Login.jsp");
        });
    }
});

function cuentaAdministrador(){
    $('#lnkCompra').show();
    $('#lnkAdm').show();
    $('#lnkLogout').show();
    $('#lnkLogin').hide();
    
    $('.BotonModificar').show();
    $('.BotonCanciones').show();
    
}
function cuentaUsuario(){
    $('#lnkCompra').show();
    $('#lnkLogout').show();
    $('#lnkLogin').hide();
    $('#lnkAdm').hide();
    
    $('.BotonModificar').hide();
    $('.BotonCanciones').show();
    
}
function cuentaNegada(){
    $('#lnkCompra').hide();
    $('#lnkLogin').show();
    $('#lnkLogout').hide();
    $('#lnkAdm').hide();
    
    $('#lnkPerfil').hide();
    $('.BotonModificar').hide();
}