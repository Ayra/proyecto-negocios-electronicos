$(document).ready(function(){
    $('.textBox').val("");
    
    $('#btnCancelar').click(function(){
        history.back();
    });
    
    $('#btnRegistrar').click(function(){
        validarDatos();
    });
});

function validarDatos(){
    var genero = $('#txtGenero').val();
    var artista = $('#txtArtista').val();
    var descripcion = $('#txtDescripcion').val();
    var precio = $('#txtPrecio').val();
    var imagen = $('#txtImagen').val();
    
    if(genero == ""){
        alert("El campo genero musical no puede estar vacio");
        $('#txtClase').focus();
    }
    else if(artista == ""){
        alert("El campo artista no puede estar vacio");
        $('#txtMarca').focus();
    }
    else if(descripcion == ""){
        alert("El campo descripcion del disco no puede estar vacio");
        $('#txtDescripcion').focus();
    }
    else if(precio == ""){
        alert("El campo precio del disco no puede estar vacio");
        $('#txtPrecio').focus();
    }
    else if(imagen == ""){
        alert("El campo disco no puede estar vacio");
        $('#txtImagen').focus();
    }
    else{
        $('#frmRegistrarDisco').submit();
    }
}