$(document).ready(function(){
    $('#btnCancelar').click(function(){
        $(location).attr('href',"MostrarA.jsp");
    });
    
    $('#btnGuardar').click(function(){
        validarDatos();
    });
});

function validarDatos(){
    var nombre = $('#txtNombre').val();
    
    if(nombre == ""){
        alert("El campo artista no puede estar vacio");
        $('#txtNombre').focus();
    }
    else{
        $('#frmModificarA').submit();
    }
}